package br.com.itau;

public class Vip extends Ingresso {

    protected double valorAdicional;
    private double valorTotal;

    public Vip(double valor, double valorAdicional, double valorTotal) {
        super(valor);
        this.valorAdicional = valorAdicional;
        this.valorTotal = valorTotal;
    }

    @Override
    public double imprimirValor() {
        return valorTotal = valor + valorAdicional;
    }
}
