package br.com.itau;

public class Ingresso {

    protected double valor;

    public Ingresso(double valor) {
        this.valor = valor;
    }

    public double imprimirValor() {
        return valor;
    }
}
