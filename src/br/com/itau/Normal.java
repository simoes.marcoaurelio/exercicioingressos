package br.com.itau;

public class Normal extends Ingresso {

    public Normal(double valor) {
        super(valor);
    }

    @Override
    public double imprimirValor() {
        return valor;
    }
}
