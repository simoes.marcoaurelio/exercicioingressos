package br.com.itau;

//import java.text.DecimalFormat;

public class Main {

    public static void main(String[] args) {

//        DecimalFormat campoDecimal = new DecimalFormat("0.00");

        Normal normal = new Normal(100.00);
        System.out.print("*** Ingresso normal ***: R$");
        System.out.println(normal.imprimirValor());

        System.out.println();

        Vip vip = new Vip (100.00, 20.00, 0.00);
        System.out.print("*** Ingresso VIP ***: R$");
        System.out.println(vip.imprimirValor());

    }
}
